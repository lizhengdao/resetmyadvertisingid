package host.stjin.resetmyadvertisingid

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.preference.PreferenceManager
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.switchmaterial.SwitchMaterial

class MainActivity : AppCompatActivity() {
    private lateinit var prefs: SharedPreferences
    private lateinit var scheduleHelper: ScheduleHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(true)
        } else {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }


        setContentView(R.layout.activity_main)
        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        scheduleHelper = ScheduleHelper(this)

        val timePicker = findViewById<TimePicker>(R.id.time_picker)
        val switch_schedule_notifications =
            findViewById<SwitchMaterial>(R.id.switch_schedule_notifications)

        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            val googleSettings = Intent("com.google.android.gms.settings.ADS_PRIVACY")
            startActivity(googleSettings)
        }


        val hour = prefs.getInt("SCHEDULE_HOUR", 0)
        val minute = prefs.getInt("SCHEDULE_MINUTE", 0)
        timePicker.hour = hour
        timePicker.minute = minute

        timePicker.setOnTimeChangedListener { picker, i, i2 ->
            prefs.edit().putInt("SCHEDULE_HOUR", picker.hour).apply()
            prefs.edit().putInt("SCHEDULE_MINUTE", picker.minute).apply()

            scheduleHelper.scheduleReminder()
        }


        switch_schedule_notifications.setOnCheckedChangeListener { compoundButton, b ->
            prefs.edit().putBoolean("SCHEDULE", b).apply()
            if (b) {
                scheduleHelper.initialSchedule(timePicker.hour, timePicker.minute)
                timePicker.visibility = View.VISIBLE
            } else {
                // Cancel future work
                WorkManager.getInstance(this).cancelAllWorkByTag(BuildConfig.APPLICATION_ID)
                timePicker.visibility = View.GONE
            }
        }
        switch_schedule_notifications.isChecked = prefs.getBoolean("SCHEDULE", false)
    }

}


